
testing of the changes logging

--------------------PL NEW--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from	rdm.ra_calibration as c
where
		isnull(raw_score,0) > 0
    and c.scc_id = @scc_id_BEH_PL
    and b.scc_id = @scc_id_BEH_PL
    and b.kp_date/100 between c.date_from and c.date_to
    and b.kp_date = @rep_date;


--------------------CC NEW--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from	rdm.ra_calibration as c
where
		isnull(raw_score,0) > 0
    and c.scc_id = @scc_id_BEH_CC
    and b.scc_id = @scc_id_BEH_CC
    and b.kp_date/100 between c.date_from and c.date_to
    and b.kp_date = @rep_date;


--------------------profit scorecard calibration--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from	rdm.ra_calibration as c
where
		b.scc_id = @scc_id_Prof_SC
    and c.scc_id = @scc_id_Prof_SC
    and b.kp_date/100 between c.date_from and c.date_to
    and b.kp_date = @rep_date;

commit work;


--------------------BEH MTG FCY--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from	rdm.ra_calibration as c
where
		isnull(raw_score,0) > 0
    and c.scc_id = @scc_id_BEH_MTG_FCY
    and b.scc_id = @scc_id_BEH_MTG_FCY
    and b.kp_date/100 between c.date_from and c.date_to
    and b.kp_date = @rep_date;

commit work;	


--------------------BEH Micro LE--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from	rdm.ra_calibration as c
where
		isnull(raw_score, 0) > 0
    and c.scc_id = @scc_id_BEH_LE
    and b.scc_id = @scc_id_BEH_LE
    and b.kp_date / 100 between c.date_from and c.date_to
    and b.kp_date = @rep_date;
commit work;	


--------------------BEH Micro SE--------------------	
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from	rdm.ra_calibration as c
where	
		isnull(raw_score, 0) > 0
    and c.scc_id = @scc_id_BEH_SE
    and b.scc_id = @scc_id_BEH_SE
    and b.kp_date / 100 between c.date_from and c.date_to
    and b.kp_date = @rep_date;

commit work;



--------------------BEH MTG LCY (initially issued in UAH)--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from
	rdm.ra_calibration	as c,
	rdm.ra_history	as his
where
		isnull(raw_score,0) > 0
    and b.kp_date	= @rep_date
	and his.kp_date	= @rep_date
    and c.scc_id = @scc_id_BEH_MTG_LCY_ISS
    and b.scc_id = @scc_id_BEH_MTG_LCY	
    and b.kp_date/100 between c.date_from and c.date_to
	and b.deal_id = his.deal_id
	and his.exclusion_flag & cast(power(2,23) as int) = 0; -- currency hasn't been changed

commit work;


--------------------BEH MTG LCY (refinanced loans)--------------------
update	rdm.ra_arc_beh_score as b 
set		score = round (raw_score * slope + intercept, 0)
from
	rdm.ra_calibration	as c,
	rdm.ra_history	as his
where
		isnull(raw_score,0) > 0
    and b.kp_date	= @rep_date
	and his.kp_date	= @rep_date
    and c.scc_id = @scc_id_BEH_MTG_LCY_REF
    and b.scc_id = @scc_id_BEH_MTG_LCY
    and b.kp_date/100 between c.date_from and c.date_to
	and b.deal_id = his.deal_id
	and his.exclusion_flag & cast(power(2,23) as int) > 0; -- currency has been changed

commit work;
